;;; machine.el --- Easy per-machine configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2021--2022 Case Duckworth
;; This file is NOT part of GNU Emacs.

;; Author: Case Duckworth <acdw@acdw.net>
;; URL: https://codeberg.org/acdw/machine.el
;; License: Fair
;; Version: 1.0
;; Package-Requires: ((emacs "25.1"))

;;; Commentary:

;; There are many solutions to loading machine-dependant code in Emacs.  These
;; include guarding forms behind `system-type' checks, `system-name' checks, and
;; others.  This library attempts to streamline and de-ad-hoc-ify that process,
;; for an easier time of per-machine configuration.

;; I wrote this package largely to scratch my own itch, so it may be somewhat
;; idiosyncratic.  In particular, I have some ease-of-life `defcustoms' and
;; functions for defining fonts (which I actually don't currently use).  Of
;; course, patches, issues, and requests are welcome.

;;; Code:

(require 'cl-lib)

(defgroup machine nil
  "Per-machine configuration."
  :group 'emacs
  :prefix "machine-")

(defcustom machine-load-directory (locate-user-emacs-file "machines"
                                                          "~/.emacs-machines")
  "The directory where per-machine configurations live."
  :type 'file)

(defcustom machine-load-order '(:type :name :user)
  "The order of per-machine configuration types to load.
The elements of this list correspond to the keys in
`machine-machines', which see.

Note that files loaded later can overwrite settings loaded
earlier."
  :type '(list (const :tag "System type" :type)
               (const :tag "System name" :name)
               (const :tag "Current user" :user)))

(define-obsolete-variable-alias 'machine-files-order
  'machine-load-order "1.0")

;;; Per-machine fonts
;; These are best-guess defaults.

(defcustom machine-default-face-font (cond
                                 ((memq system-type '(ms-dos windows-nt))
                                  "Consolas")
                                 (t "monospace"))
  "The per-machine font used for the `default' face."
  :type 'string)

(define-obsolete-variable-alias 'machine-default-font
  'machine-default-face-font "1.0")

(defcustom machine-default-face-height 100
  "The per-machine height used for the `default' face."
  :type 'number)

(define-obsolete-variable-alias 'machine-default-height
  'machine-default-face-height "1.0")

(defcustom machine-variable-pitch-face-font (cond
                                             ((memq system-type '(ms-dos windows-nt))
                                              "Arial")
                                             (t "sans-serif"))
  "The per-machine font used for the `variable-pitch' face."
  :type 'string)

(define-obsolete-variable-alias 'machine-variable-pitch-font
  'machine-variable-pitch-face-font "1.0")

(defcustom machine-variable-pitch-face-height 1.0
  "The per-machine height for the `variable-pitch' face.
A floating point number is recommended for a relative height
relative to the `default' face."
  :type 'number)

(define-obsolete-variable-alias 'machine-variable-pitch-height
  'machine-variable-pitch-face-height "1.0")

;;; Theme hooks

(defcustom machine-after-load-theme-hook nil
  "Hook functions to run after loading a theme."
  :type 'hook)

;;; File names
;; I don't know why these would be changed, but they're customizable
;; anyway.

(defcustom machine-file-name-bad-chars "[#%&{}\$!'\":@<>*?/ \r\n\t+`|=]+"
  "Characters to disallow in \"safe\" file names."
  :type 'regexp)

(defcustom machine-file-name-bad-char-replacement "-"
  "The string replacing bad characters when sanitizing file names."
  :type 'string)

;;; State variables

(defvar machine--attributes nil
  "A plist of the current machine's attributes.
The keys and their values are as follows:

- :name - function `system-name'
- :type - `system-type'
- :user - function `user-login-name'

When `machine--attributes-get' is called, this variable is
populated with the variables above.  E.g., on a Windows
machine named \"treebeard\", the user Bob's `machine--attributes' will be

    '(:name \"treebeard\"
      :type windows-nt
      :user \"bob\")

Do not set this variable yourself; call `machine--attributes-get'
to populate it instead.")

(define-obsolete-variable-alias 'machine-machines 'machine--attributes "1.0")

(defvar machine--files nil
  "The list of files to load for machine-specific configuration.
This list is populated by checking the machine's attributes and
converting those values to safe filenames using
`machine--safe-name'.

Do not set this variable yourself; call `machine--files-get'
to populate it instead.")

(define-obsolete-variable-alias 'machine-files
  'machine--files "1.0")


;;; Functions

(defun machine--warn (message &rest args)
  "Display a warning message about per-machine settings loading.
MESSAGE and ARGS are passed to `format-message'."
  (display-warning 'machine (apply #'format-message message args)))

(defun machine--safe (string)
  "Make STRING safe for a file name on all systems."
  (downcase (string-trim
             (replace-regexp-in-string machine-file-name-bad-chars
                                       machine-file-name-bad-char-replacement
                                       string)
             machine-file-name-bad-char-replacement
             machine-file-name-bad-char-replacement)))

(defun machine--run-after-load-theme-hook (&rest _) ; for advising
  "Run the hooks in `machine-after-load-theme-hook'."
  (run-hooks 'machine-after-load-theme-hook))

(defun machine--attributes-get ()
  "Determine the current machine's attributes.
Use this function to update `machine--attributes'.  The updated
`machine--attrubutes' is returned."
  (setq machine--attributes (list :name (system-name)
                                  :type system-type
                                  :user (user-login-name))))

(define-obsolete-function-alias 'machine-get-machines 'machine--attributes-get "1.0")

(defun machine--files-get ()
  "Determine the current machine's configuration files.
This function returns a list of files' canonical paths that meet
the following criterion:

- They are named for one of the values in `machine--attributes',
  which see.  The file's names can either be the plain name,
  e.g. \"bob.el\", or the name prepended with the type,
  e.g. \"user-bob.el\".  This second format serves to
  differentiate machines that have the same name in different
  \"slots,\" for example if a Linux machine's hostname was simply
  \"gnu-linux.\"

NOTE that the filenames in this list are sanitized to be valid
filenames in all filesystems.  They're sanitized by replacing
every character in `machine-file-name-bad-chars' with
`machine-file-name-bad-char-replacement' and trimming extra
replacement characters off the end of the file.  With default
settings, this would translate \"gnu/linux\" to \"gnu-linux\",
for example.

This function updates both `machine--attributes' and
`machine--files', and returns the latter."
  (machine--attributes-get)
  (setq machine-files
        (cl-loop
         for prop in machine-load-order
         collect
         (let* ((val (plist-get machine--attributes prop))
                (safe-val (machine--safe (format "%s.el" val)))
                (safe-prop-val (machine--safe (format "%s-%s.el" prop val))))
           (list (expand-file-name safe-val
                                   machine-load-directory)
                 (expand-file-name safe-prop-val
                                   machine-load-directory))))))

(define-obsolete-function-alias 'machine-get-files 'machine--files-get "1.0")

;;;###autoload
(defun machine-settings-load (&optional error nomessage)
  "Load per-machine settings from `machine-files'.
Each sublist of `machine-files' is examined file-by-file; the
first file found in each list will be loaded.

If ERROR is nil, the user will be warned if no files matching the
current machine are found, or if the current machine's attributes
can't be determined.  If ERROR is t, these warnings are elevated
to errors.  If ERROR is any other value, the errors are ignored
completely.

NOMESSAGE is passed directly to `load'."
  (machine--files-get)
  (let ((err))
    (if machine-files
        (unless (cl-loop for class in machine-files
                         collect (cl-loop for file in class
                                          thereis (load file t nomessage)))
          (setq err "Couldn't load per-machine configuration."))
      (setq err "Couldn't determine which machine is being used."))
    (if err
        (funcall (cond ((eq error) #'error)
                       ((null error) #'machine--warn)
                       (t #'ignore))
                 err)
      (machine--run-after-load-theme-hook)
      (advice-add 'load-theme :after #'machine--run-after-load-theme-hook))))

(provide 'machine)
;;; machine.el ends here
